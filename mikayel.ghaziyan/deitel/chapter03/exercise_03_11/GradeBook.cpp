#include "GradeBook.hpp"
#include <iostream>
#include <string>


GradeBook::GradeBook(std::string className, std::string teacherName)
{
    setCourseName(className);
    setTeacherName(teacherName);
}

void 
GradeBook::setCourseName(std::string className)
{
    if (className.length() > 25) {

	courseName_ = className.substr(0, 25);

	std::cout << "Name \"" << className << "\" exceeds the length.\n" << "The course name is limited to first 25 characters.\n" << std::endl; 

	return;
    }	

    courseName_ = className;
}
    
void 
GradeBook::setTeacherName(std::string teacherName)
{
    if (teacherName.length() > 25) { 

	proffName_ = teacherName.substr(0, 25);

	std::cout << "Name \"" << teacherName << "\" exceeds the length.\n"<< "Limiting the name to 25 characters.\n" << std::endl;

	return; 	
    }

    proffName_ = teacherName;
}

std::string 
GradeBook::getCourseName()
{
    return courseName_;
}

std::string 
GradeBook::getTeacherName()
{
    return proffName_;
}

void
GradeBook::displayMessage()
{
    std::cout << "Welcome to the course " << getCourseName() << ". This course is presented by: " << getTeacherName() << "." << std::endl; 
}
