a) false : all operators are evaluated from left to right except = operator
b) true
c) false : the right version is a = 5
d) false : operators will evaluate from left to right only if they have equal priority.
e) false : variables name must start with letter.
