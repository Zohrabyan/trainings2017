#include <iostream>

int 
main()
{
    int number;
    std::cout << "Please enter a five digit number: \n";
    std::cin >> number;

    if (number < 10000){
        std::cout << "Error 1: isn't 5 digit number";
        return 1;
    }

    if (number > 99999){
        std::cout << "Error 1: isn't 5 digit number ";
        return 1;
    }

    int a = number / 10000;
    int b = number / 1000 % 10;
    int c = number / 100 % 10;
    int d = number / 10 % 10;
    int e = number % 10;

    std::cout << a << "   " << b << "   " << c << "   " << d << "  " << e << "\n" ;
    return 0;
}
