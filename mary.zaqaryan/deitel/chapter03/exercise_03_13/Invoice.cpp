#include "Invoice.hpp"
#include <iostream>
#include <string>

Invoice::Invoice (std::string number, std::string description, int quantity, int price)
{
    setNumber(number);
    setDescription(description);
    setQuantity(quantity);
    setPrice(price);
}

void
Invoice::setNumber(std::string number)
{
    number_ = number;
}

std::string 
Invoice::getNumber()
{
    return number_;
}

void
Invoice::setDescription(std::string description)
{
    description_ = description;
}

std::string
Invoice::getDescription()
{
    return description_;
}

void 
Invoice::setQuantity(int quantity)
{
    if (quantity < 0) {
        std::cout << "Warning 1: Quantity can't be non positive.\n";
        quantity_ = 0;
        return;
    }
    quantity_ = quantity;
}

int
Invoice::getQuantity()
{
    return quantity_;
}

void
Invoice::setPrice(int price)
{
    if (price < 0) {
        std::cout << "Warning 2: Price can't be non positive.\n";
        price_ = 0;
        return;
    }
    price_ = price;
}

int 
Invoice::getPrice()
{
    return price_;
}

int 
Invoice::getInvoiceAmount()
{
    int amount = getQuantity() * getPrice();
    return amount;
}
