#include "GradeBook.hpp"

#include <iostream>

int
main()
{
    GradeBook gradeBook1("Lesson 4: Introduction to Programming", "Mr. Artak");
    GradeBook gradeBook2("Lesson 6: Intro to OOP", "Mr. Artak ");

    gradeBook1.displayMessage();
    gradeBook2.displayMessage();    

    return 0;
}
