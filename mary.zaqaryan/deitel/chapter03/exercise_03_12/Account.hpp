class Account
{
public:
    Account(int initialBalance);
    void setAccountBalance(int accountBalance);
    int getAccountBalance();
    int credit(int addingAmount);
    int debit(int reducingAmount);

private:
    int accountBalance_;
};
