#include <iostream>

int 
main()
{   
    double radius;
    std::cout << "Enter radius: ";
    std::cin >> radius;
    if (radius < 0) {
        std::cerr << "Error 1: Radius can't be negative.";
        return 1;
    }
    double numberPi = 3.14159;
    double diameter = 2 * radius;
    double circumference = 2 * numberPi * radius;
    double area = 2 * numberPi * radius * radius;

    std::cout << "Radius is: " << radius << std::endl;
    std::cout << "Diameter is: " << diameter << std::endl;
    std::cout << "Circumference is: " << circumference << std::endl;
    std::cout << "Area is: " << area << std::endl;
    return 0; 
}
