#include <iostream>

int 
main()
{   
    int binaryNumber;

    std::cout << "Enter binary integer number: ";
    std::cin >> binaryNumber;

    int decimalNumber = 0;
    int digitCounter = 1;

    while (binaryNumber != 0) {
        int digit = binaryNumber % 10;
        if (digit != 0) {
            if (digit != 1) {
                std::cerr << "Error 1: This number is not binary.";
                return 1;
            }
        }
        decimalNumber += digitCounter * digit;
        binaryNumber /= 10;
        digitCounter *= 2;
    }
    
    std::cout << "The decimal equivalent is " << decimalNumber;
    return 0; 
}
