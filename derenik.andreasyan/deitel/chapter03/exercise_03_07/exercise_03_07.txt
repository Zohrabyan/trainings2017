An object has attributes that are carried with the object as it is used in a program . These attributes are specified as part of the object's class. For
example, a bank account object has a balance attribute that represents the amount of money in the account. Each bank account object knows the balance in the
account it represents, but not the balances of the other accounts in the bank . Attributes are specified by the class's data members.
