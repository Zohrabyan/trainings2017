Any constructor that tak es no arguments is called a default constructor. A class gets a default constructor in one of two ways:

1. The compiler implicitly creates a default constructor in a class that does not define a constructor. Such a default constructor does not initialize the class's
data members, but does call the default constructor for each data member that is an object of another class. [Note: An uninitialized variable typically
contains a "garbage" value (e.g., an uninitialized int variable might contain -858993460 , which is likely to be an incorrect value for that variable in most
programs).

2. The programmer explicitly defines a constructor that tak es no arguments. Such a default constructor will perform the initialization specified by the
programmer and will call the default constructor for each data member that is an object of another class.

If the programmer defines a constructor with arguments, C++ will not implicitly create a default constructor for that class.
