Fatal errors cause programs to terminate immediately without having successfullyperformed their jobs. Nonfatal errors allow
programs to run to completion, often producing incorrect results
in the case of a fatal error program terminates the work and gives a message 
about the error, in this case finding the error is much easier than in the case with nonfatal errors.
