$(document).ready(function () {
    "use strict";
    var arr = [1, 5, 8, 6, 12, 4, 9];
    function filterRange(arr, a, b) {
        var newArray = [];
        for(var i = 0; i < arr.length; ++i) {
            if (a <= arr[i] && b >= arr[i]) {
                newArray.push(arr[i]);           
            }
        }
        return newArray;
    }
    var filtered = filterRange(arr, 4, 9);
    console.log(filtered);
});
