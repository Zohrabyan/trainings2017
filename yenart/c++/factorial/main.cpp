#include <iostream>
#include <cassert>

int
factorialRecursive(const int n)
{
    assert(n > 0);
    if (1 == n) {
        return n;
    }
    return n * factorialRecursive(n - 1);
}

int
factorialIterative(int n)
{
    assert(n > 0);
    int result = n;
    for (--n; n != 0; --n) {
        result *= n;
    }
    return result;
}

int
main()
{
    const int f3recursive = factorialRecursive(3);
    const int f3iterative = factorialIterative(3);
    std::cout << "Factorial Recursive: " << f3recursive << std::endl;
    std::cout << "Factorial Iterative: " << f3iterative << std::endl;
    return (f3recursive != f3iterative) ? 1 : 0;
}

