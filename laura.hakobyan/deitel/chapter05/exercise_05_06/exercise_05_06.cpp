#include <iostream>

int
main()
{
    int sum = 0;
    int count = 0;
    while (true) {
        int number;
        std::cout << "Enter numbers: ";
        std::cin >> number;
        if (9999 == number) {
            std::cout << "9999 is the sentinel value." << std::endl;
            break;
        }
        sum += number;
        ++count;
    }
    if (0 == count) {
        std::cerr << "Error 1: There is not any entered number." << std::endl;
        return 1;
    }
    double average = static_cast<double>(sum) / (count);
    std::cout << "Avarage of these numbers is " << average << std::endl;
    return 0;
}

