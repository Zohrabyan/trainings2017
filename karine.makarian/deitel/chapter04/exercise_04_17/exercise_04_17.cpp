#include <iostream>
#include <climits>

int
main()
{ 
    int counter = 1;

    std::cout << "Enter 10 numbers: ";
    double largest = INT_MIN;

    while (counter <= 10) {
        double number;

        std::cin >> number;

        if (number > largest) {
            largest = number;
        }

        ++counter;
    }

    std::cout << "The largest value is: " << largest << std::endl;

    return 0;
}

