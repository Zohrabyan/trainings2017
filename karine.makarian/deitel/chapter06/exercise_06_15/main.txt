a)With function rand() we are choosing a random number from 0 to RAND_MAX.

b)For example in casino games we need a modelling of dice rolling, card picking. Which can be done with rand() function

c)By using the function srand(), we can execute program for example 10 times, and after every executing random
number will be different. But calling srand() more than once affect the quality of randomness, because srand() is using
time to call rand().  

d)rand() function just generates a random numbers from 0 to 32767, so if we want random number from a to b we can do 
rand() % (b - a + 1) + a

e)With computer modelling we can test and gain knowledge about situations that can be in real world. So we can avoid
from some bad situations in real world using that knowledge. Also we can create simulations with computer modelling. For
example we can simulate driving. So people can learn to drive a car in virtual reality(the world of computer models.

