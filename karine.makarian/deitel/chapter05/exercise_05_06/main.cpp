#include <iostream>

int
main()
{
    int sum = 0;

    std::cout << "Enter the numbers (9999 to exit): ";
    int count = 0;    
    for ( ; ; ++count) {
        int number;
        std::cin >> number;
        
        if (9999 == number) {
            break;
        }

        sum += number;
    }
    
    double average = sum / count;
    std::cout << "Average is " << average << std::endl;

    return 0;
}

