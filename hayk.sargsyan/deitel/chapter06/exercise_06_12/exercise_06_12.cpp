#include <iostream>
#include <cmath>

double
calculateCharges(const double hours) 
{
    double amount = 2;

    if (hours > 3) {
        amount = 2 + std::ceil(hours - 3) * 0.5;
    }
    if (amount > 10) {
        amount = 10;
    }

    return amount;
}

int
main()
{
    double firstCarHours;
    
    std::cout << "Enter the parking hours of the first car: ";
    std::cin >> firstCarHours;
    
    double secondCarHours;
    
    std::cout << "Enter the parking hours of the second car: ";
    std::cin >> secondCarHours;
    
    double thirdCarHours;
    
    std::cout << "Enter the parking hours of the third car: ";
    std::cin >> thirdCarHours;
    
    const double amount1 = calculateCharges(firstCarHours);
    const double amount2 = calculateCharges(secondCarHours);
    const double amount3 = calculateCharges(thirdCarHours);

    std::cout << "Car: 1 Amount: " << amount1 << "$ Hours: " << firstCarHours << std::endl;
    std::cout << "Car: 2 Amount: " << amount2 << "$ Hours: " << secondCarHours << std::endl;
    std::cout << "Car: 3 Amount: " << amount3 << "$ Hours: " << thirdCarHours << std::endl;
    std::cout << "Total hours: " << firstCarHours + secondCarHours + thirdCarHours 
              << " Amount: " << amount1 + amount2 + amount3 << "$" << std::endl;

    return 0;
}
