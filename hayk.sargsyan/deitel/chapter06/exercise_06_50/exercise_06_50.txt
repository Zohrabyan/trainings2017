"mystery(a, b)" is the same as "multiply(a, b)". In this function program 
calculates a * b. For example if we call function like "mystery(3, 4)",
it will calculate 3 + 3 + 3 + 3 which is the same as 3 * 4

