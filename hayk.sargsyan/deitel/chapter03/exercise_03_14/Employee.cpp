#include <iostream>
#include <string>
#include "Employee.hpp"

Employee::Employee(std::string name, std::string surname, int salary)
{
    setName(name);
    setSurname(surname);
    setSalary(salary);
}

void
Employee::setName(std::string name)
{
    name_ = name;
}

std::string
Employee::getName()
{
    return name_;
}

void
Employee::setSurname(std::string surname)
{
    surname_ = surname;
}

std::string
Employee::getSurname()
{
    return surname_;
}

void
Employee::setSalary(int salary)
{
    if (salary < 0) {
        salary_ = 0;
        std::cout << "We are setting your salary to 0, because it can't be less than 0. " << std::endl;
        return;
    }
    salary_ = salary;
}

int
Employee::getSalary()
{
    return salary_;
}

