#include <iostream>
#include "Employee.hpp"

int
main()
{
    Employee employee1("Jack", "White", 11000);
    Employee employee2("Meg", "White", 9900);

    employee1.setSalary(employee1.getSalary() * 12);
    employee2.setSalary(employee2.getSalary() * 12);

    int employeeSalary1 = employee1.getSalary();
    int employeeSalary2 = employee2.getSalary();

    std::cout << "First Employee's salary: " << employeeSalary1 << std::endl;
    std::cout << "Second Employee's salary: " << employeeSalary2 << std::endl;

    employee1.setSalary(employee1.getSalary() / 10 + employee1.getSalary());
    employee2.setSalary(employee2.getSalary() / 10 + employee2.getSalary());

    int employeeNewSalary1 = employee1.getSalary();
    int employeeNewSalary2 = employee2.getSalary();

    std::cout << "First Employee's new salary: " << employeeNewSalary1 << std::endl;
    std::cout << "Second Employee's new salary: " << employeeNewSalary2 << std::endl;

    return 0;
}

