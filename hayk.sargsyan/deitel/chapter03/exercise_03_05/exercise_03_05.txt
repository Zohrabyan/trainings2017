___________________________________________________________________

Function prototype tells us
    1)The function name.
        For example sum 
    2)Type
        for example public.
    3)What it will return
        For example integer number.
    4)Attributes that we will give when calling this function 
        For example two integers, number1 and number2.
___________________________________________________________________

Function prototype example
______________________________

public int sum(int number1, int number2);
___________________________________________________________________

///////////////////////////////////////////////////////////////////
___________________________________________________________________

Function declaration declares all the function according it's name,
type, return type, attributes, function body, what is going on in 
function body...
___________________________________________________________________

Function declaration example
______________________________

int sum(int number1, int number2) {
    int sum = number1 + number2;
    return sum;
}
___________________________________________________________________
