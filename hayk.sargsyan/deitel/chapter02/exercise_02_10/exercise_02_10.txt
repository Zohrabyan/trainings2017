a)False - 
	There are operations that are evaluated from left to right, and there are operations that are evaluated from right to left.
b)True - 
	All variables begin with underscores and letters, also they are not keywords.
c)False -
	This statement is only printing "a = 5;".
d)False -
	*, / and % have higher precedence, than + and -.
e)False -
	h22 is correct variable name.
