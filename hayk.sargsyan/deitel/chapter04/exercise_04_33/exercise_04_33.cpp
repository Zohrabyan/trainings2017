#include <iostream>

int
main()
{
    double sideA;
    
    std::cout << "Enter the first side of triangle: ";
    std::cin >> sideA;

    if (sideA <= 0) {
        std::cerr << "Error 1: Triangle side can't be zero or negative." << std::endl;

        return 1;
    }
    
    double sideB;
    
    std::cout << "Enter the second side of triangle: ";
    std::cin >> sideB;
    
    if (sideB <= 0) {
        std::cerr << "Error 1: Triangle side can't be zero or negative." << std::endl;

        return 1;
    }

    double sideC;
    
    std::cout << "Enter the third side of triangle: ";
    std::cin >> sideC;
    
    if (sideC <= 0) {
        std::cerr << "Error 1: Triangle side can't be zero or negative." << std::endl;

        return 1;
    }
    if (sideA > sideB) {
        if (sideA > sideC) {
            if (sideA * sideA == sideB * sideB + sideC * sideC) {
                std::cout << "It can be a right-angled triangle." << std::endl;
            
                return 0;
            }
        }  
    } 
    if (sideB > sideA) {
        if (sideB > sideC) {
            if (sideB * sideB == sideA * sideA + sideC * sideC) {
                std::cout << "It can be a right-angled triangle." << std::endl;
            
                return 0;
            }
        } 
    }
    if (sideC > sideA) {
        if (sideC > sideB) { 
            if (sideC * sideC == sideA * sideA + sideB * sideB) {
                std::cout << "It can be a right-angled triangle." << std::endl;
                
                return 0;
            } 
        }
    }

    std::cout << "It can't be a right-angled triangle." << std::endl;

    return 0;
}
