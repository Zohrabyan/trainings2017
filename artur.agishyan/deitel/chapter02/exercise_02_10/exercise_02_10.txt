1)False - 
	There are operations that are evaluated from left to right, and there are operations that are evaluated from right to left.
2)True - 
	All variables begin with underscores and letters, also they are not keywords.
3)False -
	This statement is only printing "a = 5;".
4)False -
	*, / and % have higher precedence, than + and -.
5)False -
	h22 is correct variable name.
