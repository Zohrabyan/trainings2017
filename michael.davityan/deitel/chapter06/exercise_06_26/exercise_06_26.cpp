#include <iostream>
#include <cassert>
#include <cmath>

int userClockTimeInSeconds(const int hour, const int minute, const int second);

int 
main()
{
    int hour1;
    std::cin >> hour1;
    if (hour1 < 0 || hour1 > 23) {
        std::cout << "Error 1: wrong hour." << std::endl;
        return 1;
    }
    int minute1;
    std::cin >> minute1;
    if (minute1 < 0 || minute1 > 60) {
        std::cout << "Error 1: wrong minute." << std::endl;
        return 1;
    }
    int second1;
    std::cin >> second1;
    if (second1 < 0 || second1 > 60) {
        std::cout << "Error 1: wrong second." << std::endl;
        return 1;
    }

    int hour2;
    std::cin >> hour2;
    if (hour2 < 0 || hour2 > 23) {
        std::cout << "Error 1: wrong hour." << std::endl;
        return 1;
    }
    int minute2;
    std::cin >> minute2;
    if (minute2 < 0 || minute2 > 60) {
        std::cout << "Error 1: wrong minute." << std::endl;
        return 1;
    }
    int second2;
    std::cin >> second2;
    if (second2 < 0 || second2 > 60) {
        std::cout << "Error 1: wrong second." << std::endl;
        return 1;
    }

    std::cout << "Your  first inserted time is: " << hour1 << ":" << minute1 << ":" << second1 << "\n"
              << "Your second inserted time is: " << hour2 << ":" << minute2 << ":" << second2 << "\n";
    const int time1 = userClockTimeInSeconds(hour1, minute1, second1);
    const int time2 = userClockTimeInSeconds(hour2, minute2, second2);
    std::cout << "Your  first inserted time after the last 12 hitting(in seconds) is: " << time1 << "\n"
              << "Your second inserted time after the last 12 hitting(in seconds) is: " << time2 << "\n";
    std::cout << "The difference between your two inserted time is: " << std::abs(time2-time1) << std::endl;

    return 0;
}

int
userClockTimeInSeconds(const int hour, const int minute, const int second) 
{
    assert((hour >= 0 && hour <= 23) && (minute >= 0 && minute <= 60) && (second >= 0 && second <= 60));
    return hour * 3600 + minute * 60 + second;
}
