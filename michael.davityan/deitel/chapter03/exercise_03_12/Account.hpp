class Account
{
public:
    Account(int initialBalance);
    void setBalance(int initialBalance);
    int getBalance();
    int credit(int creditValue);
    int debit(int debitValue);
private:
    int balance_;
}; 
