#include "Invoice.hpp"
#include <iostream>
#include <string>
  
Invoice::Invoice(int number, std::string description, int quantity, int price)
{
    setPartNumber(number);
    setPartDescription(description);
    setItemQuantity(quantity);
    setPricePerItem(price);
}

void 
Invoice::setPartNumber(int number)
{
    partNumber_ = number; 
}

void 
Invoice::setPartDescription(std::string description)
{
    partDescription_ = description;
}

void 
Invoice::setItemQuantity(int quantity)
{
    if (quantity < 0) {
        std::cout << "Info 1: wrong quantity value." << std::endl;
        return;
    }
    itemQuantity_ = quantity;
}

void 
Invoice::setPricePerItem(int price)
{
    pricePerItem_ = price;
}

int 
Invoice::getPartNumber()
{
    return partNumber_; 
}

std::string 
Invoice::getPartDescription()
{
    return partDescription_;
}

int 
Invoice::getItemQuantity()
{
    return itemQuantity_;
} 

int 
Invoice::getPricePerItem()
{
    return pricePerItem_;
}

int 
Invoice::getInvoiceAmount()
{
    return itemQuantity_ * pricePerItem_;
}
