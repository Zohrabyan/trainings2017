#include <iostream>

int
main()
{
    /// These all variables are triangle sides.
    double triangleSide1;
    double triangleSide2;
    double triangleSide3;

    std::cin >> triangleSide1 >> triangleSide2 >> triangleSide3;

    if (triangleSide1 <= 0) {
        std::cout << "Error 1: invalid triangle side.";
        return 1;
    }
    if (triangleSide2 <= 0) {
        std::cout << "Error 1: invalid triangle side.";
        return 1;
    }
    if (triangleSide3 <= 0) {
        std::cout << "Error 1: invalid triangle side.";
        return 1;
    }

    if (triangleSide1 + triangleSide2 > triangleSide3) {
        if (triangleSide1 + triangleSide3 > triangleSide2) {
            if (triangleSide3 + triangleSide2 > triangleSide1) {
                std::cout << "This sides are triangle sides." << std::endl;
                return 0;
            }
        }
    }

    std::cout << "this sides are not triangle sides." << std::endl;
    return 0;
}
