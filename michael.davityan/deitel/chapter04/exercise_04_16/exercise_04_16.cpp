#include <iostream>
#include <iomanip>
#include <unistd.h>

int
main()
{
    while (true) {
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Enter hours worked (-1 to end): ";
        }
        int workingHours;
        std::cin >> workingHours;
        if (-1 == workingHours) {
            return 0;
        }
        
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Enter hourly rate of the worker ($00.00): ";
        }
        double hourlyRate;
        std::cin >> hourlyRate;
        std::cout << "Salary is $" << std::setprecision(2) << std::fixed; 
        std::cout << hourlyRate * (workingHours > 40 ? 1.5 * workingHours - 20 : workingHours); 
        std::cout << "\n" << std::endl;        
    }

    return 0;
}
