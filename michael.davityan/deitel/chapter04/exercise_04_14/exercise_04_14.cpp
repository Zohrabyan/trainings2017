#include <iostream>
#include <iomanip>
#include <unistd.h>

int
main()
{

    while (true) {
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Enter account number (-1 to end): ";
        }    
        
        int accountNumber;
        std::cin >> accountNumber;
        if (-1 == accountNumber) {
            return 0;
        }
        if (accountNumber < 0) {
            std::cout << "Error 1: wrong account number." << std::endl;
            return 1;
        }

        if (::isatty(STDIN_FILENO)) {
            std::cout << "Enter beginning balance: ";
        }                        

        double beginningBalance;
        std::cin >> beginningBalance;
        if (beginningBalance < 0) {
            std::cout << "Error 2: wrong balance.";
            return 2;
        }
        
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Enter total charges: ";
        }                        

        double totalCharges;
        std::cin >> totalCharges;
        if (totalCharges < 0) {
            std::cout << "Error 3: wrong total charge.";
            return 3;
        }

        if (::isatty(STDIN_FILENO)) {
            std::cout << "Enter total credits: ";
        }

        double totalCredits;
        std::cin >> totalCredits;
        if (totalCredits < 0) {
            std::cout << "Error 4: wrong total credit.";
            return 4;
        }

        if (::isatty(STDIN_FILENO)) {
            std::cout << "Enter credit limit: ";
        }

        double creditLimit;
        std::cin >> creditLimit;
        if (creditLimit < 0) {
            std::cout << "Error 5: wrong credit limit.";
            return 5;
        }

        double newBallance = beginningBalance + totalCharges - totalCredits;
        if (newBallance > creditLimit) {
            std::cout << "New balance is: " << std::setprecision(2) << std::fixed << newBallance << "\n";
            std::cout << "Account: " << std::setprecision(2) << std::fixed <<accountNumber << "\n";
            std::cout << "Credit limit: " << std::setprecision(2) << std::fixed << creditLimit << "\n";
            std::cout << "Ballance: " << newBallance << "\n"
                      << "Credit Limit Exceeded." << "\n" << std::endl; 
        } else {
            std::cout << "New balance is: " << newBallance << "\n" << std::endl; 
        }
    }

    return 0;
}
