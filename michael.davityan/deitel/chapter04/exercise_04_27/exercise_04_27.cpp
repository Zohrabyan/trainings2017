#include <iostream>
#include <unistd.h>

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input binary number to see it's decimal equivalent: ";
    }
    long int number;
    std::cin >> number;
    long int decimal = 0;
    long int binaryPosition = 1;
    while (number != 0) {
        decimal += binaryPosition * (number % 10);
        number /= 10;
        binaryPosition *= 2;
    }
    std::cout << "Decimal equivalent for this binary is: " << decimal << std::endl;
    return 0;
}

