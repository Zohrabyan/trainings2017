#include <iostream>
#include <climits>

int
main()
{
    int counter = 1, firstLargest = INT_MIN, secondLargest = INT_MIN, firstMaxNumber = 1, secondMaxNumber = 1;

    while (counter <= 10) {
        int number;
        std::cout << "Enter number " << counter << ": ";
        std::cin  >> number;
        
        if(firstLargest < number) {
            secondLargest = firstLargest;
            secondMaxNumber = firstMaxNumber;
            firstLargest = number;
            firstMaxNumber = counter;
        } else if (secondLargest < number) {
            secondLargest = number;
            secondMaxNumber = counter;
        }
        ++counter;
    }

    std::cout << "First max number "   << firstMaxNumber  << ": " << firstLargest  << std::endl;
    std::cout << "Second max number "  << secondMaxNumber << ": " << secondLargest << std::endl;
    return 0;    
}
