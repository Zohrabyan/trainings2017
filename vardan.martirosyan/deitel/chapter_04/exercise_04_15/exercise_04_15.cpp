#include <iostream>

int
main()
{
    while (true) {
        double sales;
        std::cout << "Enter sales in dollars (-1 to end): ";
        std::cin  >> sales;

        if (-1 == sales) {
            std::cout << std::endl;
            return 0;
        }

        if (sales < 0) {
            std::cerr << "Error 1: Sales can not be negative."<< std::endl;
            return 1;
        }

        double salary = ((sales * 9 / 100) + 200);
        std::cout << "Salary is: $" << salary << std::endl << std::endl;
    }
    std::cerr << "Error 2: Loop while does not work." << std::endl;
    return 2;
}
