#include <iostream>

int
main()
{
    double allMilesDriven = 0, allGallons = 0;
        
    while (true) {
        double milesDriven;
        std::cout << "Enter the miles used (-1 to quit): ";
        std::cin  >> milesDriven;
        
        if (-1 == milesDriven) {
            std::cout << std::endl;
            return 0;
        }

        if (milesDriven < 0) {
            std::cerr << "Error 1: Miles can not be negative." << std::endl;
            return 1;
        }

        double gallons;
        std::cout << "Enter gallons: ";
        std::cin  >> gallons;
        
        if (gallons < -1) {
            std::cerr << "Error 2: Gllons can not be negative." << std::endl;
            return 2;
        }
        std::cout << "MPG this tankful: ";
        double mpg = milesDriven / gallons;
        std::cout << mpg << std::endl;

        allMilesDriven += milesDriven;
        allGallons += gallons;
        double totalMpg = allMilesDriven / allGallons;
        std::cout << "Total MPG: " << totalMpg << std::endl << std::endl;
    }
    std::cerr << "Error 3: Loop while does not worded." << std::endl;
    return 3;
}

