#include <iostream>
#include "Analysis.hpp" 

void Analysis::processExamResults() 
{ 
    int passes = 0; 
    int failures = 0; 
    int studentCounter = 1;
    int result; 

    while (studentCounter <= 10) { 
        std::cout << "Enter result (1 = pass, 2 = fail): "; 
        std::cin >> result; 
        ++studentCounter; 
        if (1 == result) {
            ++passes; 
        } else if (2 == result) {
            ++failures;
        } else {
            std::cout << "Warning: The result should be either 1 = pass or 2 = fail." << std::endl;
            --studentCounter; 
        }
    }
    std::cout << "Passed " << passes << "\nFailed " << failures << std::endl; 

    if (passes > 8) { 
        std::cout << "Raise tuition" << std::endl; 
    }
}

