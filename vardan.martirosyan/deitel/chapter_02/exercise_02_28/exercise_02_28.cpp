#include <iostream>

int
main()
{
    int fiveDigitNumber; 

    std::cout << "Enter five digit number: ";
    std::cin >> fiveDigitNumber; 

    if (number > 99999) {
        std::cout << "Error 1. Not a five digit number!" << std::endl;
        return 1;
    }
    if (number < 10000) {
         std::cout << "Error 1. Not a five digit number!" << std::endl;
         return 1;
    }    

    int digit1 =  fiveDigitNumber / 10000; 
    int digit2 = (fiveDigitNumber / 1000) % 10; 
    int digit3 = (fiveDigitNumber / 100) % 10;
    int digit4 = (fiveDigitNumber / 10) % 10;
    int digit5 =  fiveDigitNumber % 10;

    std::cout << digit1 << " " << digit2 << " " << digit3 << " " << digit4 << " " << digit5 << std::endl;

    return 0;
}

