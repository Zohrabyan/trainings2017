#include <iostream>
#include <iomanip>
#include <cmath>
#include <cassert>

double calculateCharges(const double time);

int
main()
{
    double totalTime = 0, totalCharge = 0;
    for (int carNumber = 1; carNumber <= 3; ++carNumber) {
        double hours;
        std::cout << "Enter car parking hours: ";
        std::cin >> hours;
        if (std::cin.fail()){
            std::cerr << "Error 2: Invalid input, your input is not a digit. " << std::endl;
            return 2;
        }
        if (hours < 0 || hours > 24) {
            std::cerr << "Error 1: Invalid number." << std::endl;
            return 1;
        }
        const double charge = calculateCharges(hours);
        std::cout << "Car" << "\t" << "Hours" << "\t" << "Charge" << std::endl;
        std::cout << carNumber << "\t" << hours << "\t" << charge << std::endl;

        totalTime += hours;
        totalCharge += charge;
    }
    std::cout << "Total\t" << totalTime << "\t" << totalCharge << std::endl;

    return 0;
}

double
calculateCharges(const double time)
{
    assert(time > 0 || time < 24);

    double charge = 2;
    int parkingTime = std::ceil(time);
    if (parkingTime >= 3) {
        charge = (2 + ((parkingTime - 3) * 0.5)); 
    } 
    if (charge > 10 ) {
        charge = 10;
    }
    return charge;
}

