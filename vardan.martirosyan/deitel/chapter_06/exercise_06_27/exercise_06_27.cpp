#include <iostream>

double
fahrenheit(const double temperature)
{ 
    return temperature * 9 / 5 + 32;
}

double
celsius(const double temperature)
{ 
    return (temperature - 32) * 5 / 9;
}

int
main()
{
    std::cout << "Celsius to Fahrenheit.\n" << std::endl;
    for (int temperature = 0; temperature <= 100; ++temperature) {
        std::cout << "Ceslius is " << temperature << "\tFahrenheit is " << fahrenheit(temperature) << std::endl;
    }
    std::cout << "Fahrenheit to Celsius.\n" << std::endl;
    for (int temperature = 32; temperature <= 212; ++temperature) {
        std::cout << "Fahrenheit is " << temperature << "\tCelsius is " << celsius(temperature) << std::endl;
    }
    return 0;
}

