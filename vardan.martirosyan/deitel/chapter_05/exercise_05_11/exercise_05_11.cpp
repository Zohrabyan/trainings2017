#include <iostream>
#include <iomanip>

int main()
{
    std::cout << "Year" << std::setw(21) << "Amount on deposit" << std::endl;
    std::cout << std::fixed << std::setprecision(2);

    for (int rate = 5; rate <= 10; ++rate) {
        std::cout << "Rate is " << rate << "%" << std::endl;
        double principal = 1000.0; 
        double percent = 1 + (rate / 100.0);
        for (int year = 1; year <= 10; ++year) {
            principal *= percent;
            std::cout << std::setw(4) << year << std::setw(21) << principal << std::endl;
        } 
        std::cout << std::endl;
    }
    return 0; 
}

