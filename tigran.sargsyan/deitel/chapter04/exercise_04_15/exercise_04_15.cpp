#include <iostream>
#include <cmath>
#include <float.h>

int
main()
{
    while (true) {
        double sales;
        std::cout << "Enter sales in dollars (-1 to quit): ";
        std::cin >> sales;
        if (fabs(-1 - sales) < DBL_EPSILON) {
            return 0;
        }
        if (sales < 0) {
            std::cout << "Error 1. Sales should be positive." << std::endl;
            return 1;
        }

        double salary = 200 + 0.09 * sales;
        std::cout << "Salary is: $" << salary << "\n" << std::endl;
    }

    return 0;
}

