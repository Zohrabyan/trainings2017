#include <iostream>

int
main()
{
    int integer = 0;
    
    std::cout << "Enter a five-digit integer: ";
    std::cin  >> integer;

    if (integer > 99999) {
        std::cerr << "Error 1: The number is not five-digit!" << std::endl;
        return 1;
    }
    if (integer < 10000) {
        std::cerr << "Error 2: The number is not five-digit!" << std::endl;
        return 2;
    }

    int digit1 = integer / 10000;
    int digit2 = integer / 1000 % 10;
    int digit4 = integer / 10 % 10;
    int digit5 = integer % 10;

    if (digit1 == digit5) {
        if (digit2 == digit4) {
            std::cout << "The number " << integer << " is palindrome!" << std::endl;
            return 0;
        }
    }
    std::cout << "The number " << integer << " is not palindrome!" << std::endl;
    return 0;
}
