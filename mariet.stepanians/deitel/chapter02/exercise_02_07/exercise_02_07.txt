std::cout <<   formats values into a readable form, and streams them to the standard 
               output stream, which is called cout and, like most of the standard library,
               scoped inside a namespace called std.

std::cin  >>   inputs values from keybaord, and streams them to the standard input stream,
               which is called cin and, like most of the standard library,
               scoped inside a namespace called std.
